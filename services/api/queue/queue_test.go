package queue_test

import (
	"testing"

	miniredis "github.com/alicebob/miniredis/v2"
	"github.com/gomodule/redigo/redis"
	"github.com/stretchr/testify/assert"
	"gitlab.com/aonahara/archive-next/services/api/queue"
)

func TestQueue(t *testing.T) {
	// Set up redis
	mr := miniredis.RunT(t)
	defer mr.Close()

	pool := &redis.Pool{
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", mr.Addr())
		},
	}
	defer pool.Close()

	// Set up queue
	q := queue.NewQueue(pool, "test")
	assert.Equal(t, 0, q.Len())

	// Push
	q.Push(&queue.TaskData{
		Source:   queue.TaskSourceYouTube,
		Type:     queue.TaskTypeVOD,
		Url:      "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
		Priority: 1,
	})
	assert.Equal(t, 1, q.Len())

	// Push another one
	q.Push(&queue.TaskData{
		Source:   queue.TaskSourceTwitch,
		Type:     queue.TaskTypeLive,
		Url:      "https://www.twitch.tv/kitsune_cw",
		Priority: 5,
	})
	assert.Equal(t, 2, q.Len())

	// And another one
	q.Push(&queue.TaskData{
		Source:   "YouTube",
		Type:     "video",
		Url:      "https://www.twitch.tv/kitsune_cw",
		Priority: 5,
	})
}
