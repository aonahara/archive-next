package queue

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"time"

	"github.com/gomodule/redigo/redis"
)

// Enum of task status
const (
	// Task is waiting to be processed
	TaskStatusIdle = "idle"
	// Task is being processed
	TaskStatusRunning = "running"
	// Task is completed
	TaskStatusDone = "done"
	// Task has failed and will be retried
	TaskStatusFailing = "failing"
	// Task has failed and will not be retried
	TaskStatusFailed = "failed"

	TaskSourceYouTube = "youtube"
	TaskSourceTwitch  = "twitch"

	TaskTypeVOD  = "vod"
	TaskTypeLive = "live"
)

type Task struct {
	// Unique identifier for the task.
	Id string
	// Date and time when the task was created.
	CreatedAt int64
	// Status of the task.
	// One of: idle, running, done, failing, failed.
	Status string
	// List of workers that have attempted to run the task.
	Workers []string
	// The task's payload.
	Payload TaskData
}

type TaskData struct {
	// e.g. YouTube, Twitch, etc.
	Source string
	// e.g. vod, live, etc.
	Type string
	// e.g. "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
	Url string
	// Priority. A larger number means a higher priority.
	Priority int64
}

type Queue interface {
	// push a task to the queue
	Push(payload *TaskData) (*Task, error)
	// get the length of the queue
	Len() int
	// request a task from the queue
	Request(workerId string) (*Task, error)
	// update the status of a task
	UpdateStatus(taskId string, status string) error
}

type QueueImpl struct {
	redisPool *redis.Pool
	name      string

	// Redis key names
	hKey string
	zKey string
}

// NewQueue creates a new queue
func NewQueue(redisPool *redis.Pool, name string) Queue {
	return &QueueImpl{
		redisPool: redisPool,
		name:      name,
		hKey:      name + ":h",
		zKey:      name + ":z",
	}
}

// Generate a task ID by hashing the payload
func GenerateTaskId(payload *TaskData) string {
	hasher := sha1.New()
	hasher.Write([]byte(payload.Source + payload.Type + payload.Url))
	return hex.EncodeToString(hasher.Sum(nil))
}

// CalculateTaskPriority calculates the priority of a task based on the task's
// created date, the number of workers that have processed the task, and the
// priority multiplier.
func CalculateTaskPriority(task *Task) int64 {
	// Start at 10 billion
	var priority int64 = 10_000_000_000

	// Older tasks should have a higher priority
	priority -= task.CreatedAt

	// Live tasks should have a higher priority
	if task.Payload.Type == TaskTypeLive {
		priority += 10_000_000_000
	}

	// Penalize tasks that have been processed by more workers
	priority = int64(float64(priority) / float64(len(task.Workers)))

	// Multiply by the priority multiplier
	priority *= task.Payload.Priority

	return priority
}

func (q *QueueImpl) Push(payload *TaskData) (*Task, error) {
	conn := q.redisPool.Get()
	defer conn.Close()

	// Create the task
	taskId := GenerateTaskId(payload)
	task := &Task{
		Id:        taskId,
		CreatedAt: time.Now().Unix(),
		Status:    TaskStatusIdle,
		Payload:   *payload,
	}
	priority := CalculateTaskPriority(task)

	// Serialize the task
	taskJson, err := json.Marshal(task)
	if err != nil {
		return nil, err
	}

	// Insert the task to a hash set
	if _, err = conn.Do("HSET", q.hKey, taskId, taskJson); err != nil {
		return nil, err
	}

	// Push the task to the queue
	if _, err = conn.Do("ZADD", q.zKey, priority, taskId); err != nil {
		return nil, err
	}

	return task, nil
}

func (q *QueueImpl) Len() int {
	conn := q.redisPool.Get()
	defer conn.Close()

	// Get the length of the queue
	length, err := redis.Int(conn.Do("ZCARD", q.zKey))
	if err != nil {
		return 0
	}

	return length
}

func (q *QueueImpl) Request(workerId string) (*Task, error) {
	panic("not implemented")
}

func (q *QueueImpl) UpdateStatus(taskId string, status string) error {
	panic("not implemented")
}
