package database

import (
	"errors"

	"gitlab.com/aonahara/archive-next/services/api/config"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func Connect() (*gorm.DB, error) {
	dbConfig := config.GetConfig().Database

	var dialect gorm.Dialector

	if dbConfig.Type == config.DatabaseTypeSQLite {
		dialect = sqlite.Open(dbConfig.ConnectionString)
	} else if dbConfig.Type == config.DatabaseTypePostgres {
		dialect = postgres.Open(dbConfig.ConnectionString)
	} else {
		return nil, errors.New("invalid database type: " + dbConfig.Type)
	}

	db, err := gorm.Open(dialect, &gorm.Config{})
	if err != nil {
		return nil, err
	}

	return db, nil
}
