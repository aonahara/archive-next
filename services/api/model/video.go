package model

import (
	"time"

	"gorm.io/gorm"
)

const (
	VideoDeliveryMethodLive   = "live"
	VideoDeliveryMethodUpload = "upload"
	VideoSourceYouTube        = "youtube"
)

type Video struct {
	ID        uint `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
	Metadata  Metadata       `gorm:"embedded"`
}

type Metadata struct {
	Title          string `json:"title"`
	Description    string `json:"description"`
	Source         string `json:"source"`
	OriginalURL    string `json:"original_url"`
	OriginalID     string `json:"original_id" gorm:"index"`
	DeliveryMethod string `json:"delivery_method"`
	Timestamps

	LikeCount    int `json:"like_count"`
	DislikeCount int `json:"dislike_count"`
	ViewCount    int `json:"view_count"`
}

type Timestamps struct {
	ScheduledStartTime time.Time `json:"scheduled_start_time"`
	ActualStartTime    time.Time `json:"actual_start_time"`
	ActualEndTime      time.Time `json:"actual_end_time"`
	PublishedAt        time.Time `json:"published_at"`
}
