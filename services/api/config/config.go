package config

import (
	"os"

	"gopkg.in/yaml.v2"
)

const (
	DatabaseTypeSQLite   = "sqlite"
	DatabaseTypePostgres = "postgres"
)

type Config struct {
	Controller controllerConfig `yaml:"controller"`
	Database   databaseConfig   `yaml:"database"`
}

type controllerConfig struct {
	HTTP hostPort `yaml:"http"`
}

type hostPort struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

type databaseConfig struct {
	Type             string `yaml:"type"`
	ConnectionString string `yaml:"connection_string"`
}

var config Config
var loaded bool

func GetConfig() Config {
	if !loaded {
		loadConfig()
	}

	return config
}

func loadConfig() {
	configFileName := os.Getenv("CONFIG_FILE")
	if configFileName == "" {
		configFileName = "config.yaml"
	}

	configFile, err := os.Open(configFileName)
	if err != nil {
		panic(err)
	}
	defer configFile.Close()

	decoder := yaml.NewDecoder(configFile)
	err = decoder.Decode(&config)
	if err != nil {
		panic(err)
	}
}
