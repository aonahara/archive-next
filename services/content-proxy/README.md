# content-proxy

Deliver content from Google Drive. Caching should be handled separately with a
reverse proxy such as nginx.

Environment variables:

```
LISTEN=:8080
GDRIVE_CLIENT_ID=
GDRIVE_CLIENT_SECRET=
GDRIVE_REFRESH_TOKEN=
```

Usage:

```
http://localhost:8080/gd/<rootFolderId>/path/to/file.ext
```
