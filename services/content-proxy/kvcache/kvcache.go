package kvcache

import (
	"sync"
	"time"
)

type cacheItem struct {
	Value     string
	ExpiresAt int64
}

type KVCache struct {
	cache map[string]cacheItem
	lock  sync.RWMutex
}

func NewKVCache() *KVCache {
	cache := &KVCache{
		cache: make(map[string]cacheItem),
		lock:  sync.RWMutex{},
	}

	// Goroutine to clean up expired items
	go func() {
		for now := range time.Tick(time.Second) {
			cache.lock.Lock()
			for key, item := range cache.cache {
				if item.ExpiresAt > 0 && item.ExpiresAt < now.Unix() {
					delete(cache.cache, key)
				}
			}
			cache.lock.Unlock()
		}
	}()

	return cache
}

func (k *KVCache) Get(key string) (string, bool) {
	k.lock.RLock()
	item, ok := k.cache[key]
	k.lock.RUnlock()

	if !ok {
		return "", false
	}

	if item.ExpiresAt > 0 && item.ExpiresAt < time.Now().Unix() {
		return "", false
	}

	return item.Value, true
}

func (k *KVCache) GetOrDefault(key string, defaultValue string) string {
	value, ok := k.Get(key)
	if !ok {
		return defaultValue
	}

	return value
}

func (k *KVCache) Put(key string, value string, ttl time.Duration) {
	k.lock.Lock()
	k.cache[key] = cacheItem{
		Value:     value,
		ExpiresAt: time.Now().Add(ttl).Unix(),
	}
	k.lock.Unlock()
}
