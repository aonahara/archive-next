package kvcache_test

import (
	"testing"
	"time"

	"gitlab.com/aonahara/archive-next/services/content-proxy/kvcache"
)

func TestKVCache(t *testing.T) {
	// Create a new KVCache.
	kvcache := kvcache.NewKVCache()

	// Get a value that does not exist.
	value, ok := kvcache.Get("key")
	if value != "" || ok {
		t.Errorf("Get() returned %v, %v, want %v, %v", value, ok, "", false)
	}

	// Get a value that does exist, or return a default value.
	value = kvcache.GetOrDefault("key", "default")
	if value != "default" {
		t.Errorf("GetOrDefault() returned %v, want %v", value, "default")
	}

	// Put a value.
	kvcache.Put("key", "value", 100)

	// Get the value.
	value = kvcache.GetOrDefault("key", "default")
	if value != "value" {
		t.Errorf("Get() returned %v, %v, want %v, %v", value, ok, "value", true)
	}

	// Put a value that will expire.
	kvcache.Put("key2", "value", 0)

	// Sleep for 2 seconds.
	time.Sleep(1 * time.Second)

	// Get the value.
	value, ok = kvcache.Get("key2")
	if value != "" || ok {
		t.Errorf("GetOrDefault() returned %v, %v, want %v, %v", value, ok, "", false)
	}
}
