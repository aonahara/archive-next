package gdrive_test

import (
	"context"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/aonahara/archive-next/services/content-proxy/gdrive"
	"gitlab.com/aonahara/archive-next/utils/mocks"
)

func TestGDrive(t *testing.T) {
	ctx := context.Background()
	gd := gdrive.NewGDriveClient(gdrive.GDriveClientConfig{
		ClientId:     "clientId",
		ClientSecret: "clientSecret",
		RefreshToken: "refreshToken",
	})
	gdrive.Client = &mocks.MockClient{}

	// Set up the mock HTTP client
	autoMock := mocks.NewAutoMock(t, mocks.AutoMockList{
		{ // Access token request
			Request: mocks.Request{
				Method: "POST",
				Url:    "https://www.googleapis.com/oauth2/v4/token",
				Body: []byte(
					url.Values{
						"client_id":     {"clientId"},
						"client_secret": {"clientSecret"},
						"refresh_token": {"refreshToken"},
						"grant_type":    {"refresh_token"},
					}.Encode(),
				),
				Headers: http.Header{
					"Content-Type": []string{"application/x-www-form-urlencoded"},
				},
			},
			Response: mocks.Response{
				StatusCode: 200,
				Body:       []byte(`{"access_token":"accessToken"}`),
				Headers: http.Header{
					"Content-Type": []string{"application/json"},
				},
			},
		},
		{ // Get folder ID request
			Request: mocks.Request{
				Method: "GET",
				Url: "https://www.googleapis.com/drive/v3/files?" + url.Values{
					"includeItemsFromAllDrives": {"true"},
					"supportsAllDrives":         {"true"},
					"fields":                    {"files(id)"},
					"q":                         {"name = 'someFolder' and 'rootFolderId' in parents"},
				}.Encode(),
			},
			Response: mocks.Response{
				StatusCode: 200,
				Body:       []byte(`{"files":[{"id":"idOfSomeFolder"}]}`),
				Headers: http.Header{
					"Content-Type": []string{"application/json"},
				},
			},
		},
		{ // Get file ID request
			Request: mocks.Request{
				Method: "GET",
				Url: "https://www.googleapis.com/drive/v3/files?" + url.Values{
					"includeItemsFromAllDrives": {"true"},
					"supportsAllDrives":         {"true"},
					"fields":                    {"files(id)"},
					"q":                         {"name = 'someFile' and 'idOfSomeFolder' in parents"},
				}.Encode(),
			},
			Response: mocks.Response{
				StatusCode: 200,
				Body:       []byte(`{"files":[{"id":"idOfSomeFile"}]}`),
				Headers: http.Header{
					"Content-Type": []string{"application/json"},
				},
			},
		},
		{ // Get file request
			Request: mocks.Request{
				Method: "GET",
				Url: "https://www.googleapis.com/drive/v3/files/idOfSomeFile?" + url.Values{
					"alt":                       {"media"},
					"includeItemsFromAllDrives": {"true"},
					"supportsAllDrives":         {"true"},
				}.Encode(),
				Body: nil,
			},
			Response: mocks.Response{
				StatusCode: 200,
				Body:       []byte(`test file contents`),
				Headers: http.Header{
					"Content-Type": []string{"application/json"},
				},
			},
		},
		{ // Second get file request
			Request: mocks.Request{
				Method: "GET",
				Url: "https://www.googleapis.com/drive/v3/files/idOfSomeFile?" + url.Values{
					"alt":                       {"media"},
					"includeItemsFromAllDrives": {"true"},
					"supportsAllDrives":         {"true"},
				}.Encode(),
				Body: nil,
			},
			Response: mocks.Response{
				StatusCode: 200,
				Body:       []byte(`test file contents`),
				Headers: http.Header{
					"Content-Type": []string{"application/json"},
				},
			},
		},
	})
	mocks.GetDoFunc = autoMock.Do

	resp, err := gd.GetFileByPath(ctx, "/someFolder/someFile", "rootFolderId", "123-456")
	assert.NoError(t, err)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, body, []byte(`test file contents`))

	// Get file again, most of the calls should be cached
	resp, err = gd.GetFileByPath(ctx, "/someFolder/someFile", "rootFolderId", "123-456")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, body, []byte(`test file contents`))

	// Get file with invalid filename
	id, err := gd.GetSingleFileId(ctx, "file'name", "rootFolderId")
	assert.Error(t, err)
	assert.Equal(t, id, "")

	// Get file with invalid folder ID
	id, err = gd.GetSingleFileId(ctx, "someFile", "invalidFolder'Id")
	assert.Error(t, err)
	assert.Equal(t, id, "")
}
