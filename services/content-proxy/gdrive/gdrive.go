package gdrive

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/aonahara/archive-next/services/content-proxy/kvcache"
	"gitlab.com/aonahara/archive-next/utils/mocks"
)

var (
	// Google Drive API endpoint
	ApiEndpoint = "https://www.googleapis.com"
	// HTTP client
	Client mocks.HttpClient
)

type GDriveClient struct {
	clientId     string
	clientSecret string
	refreshToken string

	accessToken          string
	accessTokenExpiresAt int64
	pathIdCache          *kvcache.KVCache
	pathCacheTtl         time.Duration
}

type GDriveClientConfig struct {
	ClientId     string
	ClientSecret string
	RefreshToken string
	PathCacheTtl time.Duration
}

// Create a new Google Drive client
func NewGDriveClient(config GDriveClientConfig) *GDriveClient {
	Client = &http.Client{}
	return &GDriveClient{
		clientId:     config.ClientId,
		clientSecret: config.ClientSecret,
		refreshToken: config.RefreshToken,
		pathIdCache:  kvcache.NewKVCache(),
		pathCacheTtl: config.PathCacheTtl,
	}
}

func (g *GDriveClient) refreshAccessToken() (string, error) {
	// Check if we have a valid access token
	if g.accessToken != "" && g.accessTokenExpiresAt > 0 {
		if g.accessTokenExpiresAt > time.Now().Unix() {
			return g.accessToken, nil
		}
	}

	// Request a new access token
	params := url.Values{}
	params.Set("refresh_token", g.refreshToken)
	params.Set("client_id", g.clientId)
	params.Set("client_secret", g.clientSecret)
	params.Set("grant_type", "refresh_token")

	buf := bytes.NewBufferString(params.Encode())
	req, err := http.NewRequest(http.MethodPost, ApiEndpoint+"/oauth2/v4/token", buf)
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := Client.Do(req)
	if err != nil {
		return "", err
	}

	var responseBody struct {
		AccessToken string `json:"access_token"`
	}
	err = json.NewDecoder(resp.Body).Decode(&responseBody)
	if err != nil {
		return "", err
	}

	// Update the access token
	g.accessToken = responseBody.AccessToken
	g.accessTokenExpiresAt = time.Now().Unix() + 3600 // 1 hour
	return g.accessToken, nil
}

// Returns the Google Drive ID of a file or folder, given its name and parent
// folder ID
func (g *GDriveClient) GetSingleFileId(ctx context.Context, fileName, parentId string) (string, error) {
	// Check if we have the file id cached
	cacheKey := parentId + "/" + fileName
	if id, ok := g.pathIdCache.Get(cacheKey); ok {
		return id, nil
	}

	// Get the access token
	accessToken, err := g.refreshAccessToken()
	if err != nil {
		return "", err
	}

	// Disallow quotes
	if strings.Contains(fileName, "'") || strings.Contains(parentId, "'") {
		return "", errors.New("Invalid file name or parent id")
	}

	// Prepare the request
	params := url.Values{
		"includeItemsFromAllDrives": {"true"},
		"supportsAllDrives":         {"true"},
		"fields":                    {"files(id)"},
		"q":                         {"name = '" + fileName + "' and '" + parentId + "' in parents"},
	}

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		ApiEndpoint+"/drive/v3/files?"+params.Encode(),
		nil,
	)
	if err != nil {
		return "", err
	}
	req.Header.Set("Authorization", "Bearer "+accessToken)

	// Send the request
	resp, err := Client.Do(req)

	// Parse the response
	var responseBody struct {
		Files []struct {
			Id string `json:"id"`
		} `json:"files"`
	}
	err = json.NewDecoder(resp.Body).Decode(&responseBody)
	if err != nil {
		return "", err
	}

	// Cache the file id
	if len(responseBody.Files) == 0 {
		return "", nil
	}

	fileId := responseBody.Files[0].Id
	g.pathIdCache.Put(cacheKey, fileId, g.pathCacheTtl)
	return fileId, nil
}

// Resolve the Google Drive ID of a file or folder, given its full path
// relative to the root folder
func (g *GDriveClient) GetFileIdRecursive(ctx context.Context, path, rootId string) (string, error) {
	// Check if we have the file id cached
	cacheKey := rootId + "/" + path
	if id, ok := g.pathIdCache.Get(cacheKey); ok {
		return id, nil
	}

	// Split the path into segments
	segments := strings.Split(path, "/")
	parentId := rootId

	// Iterate over the segments
	for _, segment := range segments {
		// Skip empty segments
		if segment == "" {
			continue
		}

		// Get the file id
		id, err := g.GetSingleFileId(ctx, segment, parentId)
		if err != nil {
			return "", err
		}

		// Update the parent id
		parentId = id
	}

	// Cache the file id
	g.pathIdCache.Put(cacheKey, parentId, g.pathCacheTtl)
	return parentId, nil
}

// Get the file from Google Drive
func (g *GDriveClient) GetFile(ctx context.Context, fileId, requestRange string) (*http.Response, error) {
	// Get the access token
	accessToken, err := g.refreshAccessToken()
	if err != nil {
		return nil, err
	}

	// Prepare the request
	params := url.Values{}
	params.Set("alt", "media")
	params.Set("supportsAllDrives", "true")
	params.Set("includeItemsFromAllDrives", "true")

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		ApiEndpoint+"/drive/v3/files/"+fileId+"?"+params.Encode(),
		nil,
	)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+accessToken)
	req.Header.Set("Range", requestRange)

	// Send the request
	return Client.Do(req)
}

func (g *GDriveClient) GetFileByPath(ctx context.Context, path, rootId, requestRange string) (*http.Response, error) {
	// Get the file id
	fileId, err := g.GetFileIdRecursive(ctx, path, rootId)
	if err != nil {
		return nil, err
	}

	// Get the file
	return g.GetFile(ctx, fileId, requestRange)
}
