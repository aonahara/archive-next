package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/rs/cors"
	envconfig "github.com/sethvargo/go-envconfig"
	"gitlab.com/aonahara/archive-next/services/content-proxy/gdrive"
)

type Config struct {
	// Google Drive
	GDriveClientId     string `env:"GDRIVE_CLIENT_ID"`
	GDriveClientSecret string `env:"GDRIVE_CLIENT_SECRET"`
	GDriveRefreshToken string `env:"GDRIVE_REFRESH_TOKEN"`

	// Web Server
	Listen string `env:"LISTEN"`
}

func main() {
	ctx := context.Background()

	// Parse config
	var cfg Config
	if err := envconfig.Process(ctx, &cfg); err != nil {
		panic(err)
	}

	// Validate config
	if cfg.GDriveClientId == "" {
		panic("GDRIVE_CLIENT_ID is required")
	}
	if cfg.GDriveClientSecret == "" {
		panic("GDRIVE_CLIENT_SECRET is required")
	}
	if cfg.GDriveRefreshToken == "" {
		panic("GDRIVE_REFRESH_TOKEN is required")
	}
	if cfg.Listen == "" {
		cfg.Listen = ":8080"
	}

	// Create the GDriveClient
	client := gdrive.NewGDriveClient(gdrive.GDriveClientConfig{
		ClientId:     cfg.GDriveClientId,
		ClientSecret: cfg.GDriveClientSecret,
		RefreshToken: cfg.GDriveRefreshToken,
	})

	// Create a new http mux
	mux := http.NewServeMux()

	// Health check
	mux.HandleFunc("/_health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	// Register the handler for the content proxy
	mux.HandleFunc("/gd/", func(w http.ResponseWriter, r *http.Request) {
		// Parse the URL in the form of /gd/<rootId>/rest/of/path
		parts := strings.Split(r.URL.Path, "/")
		if len(parts) < 3 {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "Invalid URL")
			return
		}

		rootId := parts[2]
		path := strings.Join(parts[3:], "/")

		// Get the file
		file, err := client.GetFileByPath(r.Context(), path, rootId, r.Header.Get("Range"))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Error getting file: %s", err)
			return
		}

		// Write headers
		w.WriteHeader(file.StatusCode)
		for k, v := range file.Header {
			for _, vv := range v {
				w.Header().Add(k, vv)
			}
		}

		// Stream the body
		io.Copy(w, file.Body)
	})

	// Set up cors
	handler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "HEAD"},
		AllowedHeaders: []string{"Range"},
	}).Handler(mux)

	// Start the server
	fmt.Printf("Listening on %s\n", cfg.Listen)
	err := http.ListenAndServe(cfg.Listen, handler)
	if err != nil {
		panic(err)
	}
}
