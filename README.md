# archive-next

In-progress rewrite of [Ragtag Archive](https://archive.ragtag.moe).

Right now, Ragtag Archive's codebase is in shambles, with no documentation or
proper testing. It's unmaintainable; only way for me to add a feature was to
deploy it to prod and hope that nothing breaks. This rewrite aims to unify all
of the components that make Ragtag Archive work, from the archive workers, the
task queue, the content delivery server, to the frontend, and to properly
document and test everything from the start.

The goal for this rewrite is not to recreate the current infrastructure, but to
design a new one that achieves the same goal. This means the archive will still
be limited to YouTube VOD archival.

## Implementation plan

The project will be split into several independent modules:

- Frontend / web app
- Content delivery server
- API server for fontend & worker management
- Workers

## Progress

### Shared code

- [x] Google Drive API
- [ ] Metrics and logging

### Frontend

- [ ] Landing page
- [ ] Video search
- [ ] Video player
- [ ] Metrics and logging

### Content delivery server

- [x] Content server
- [ ] Metrics and logging

### API

Frontend-related

- [ ] Search
- [ ] Get video details
- [ ] Metrics and logging

Worker management

- [ ] Task queue
- [ ] Channel monitor
- [ ] Secrets management
- [ ] Metrics and log collection

### Worker

- [ ] YouTube downloader
- [ ] Muxer
- [ ] Uploader
- [ ] Metrics and logging

## Future plans

- Leverage BitTorrent v2 to generate per-video and whole-channel torrents
