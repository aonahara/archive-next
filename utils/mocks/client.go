package mocks

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type MockClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

type Request struct {
	Method  string
	Url     string
	Body    []byte
	Headers http.Header
}
type Response struct {
	StatusCode int
	Body       []byte
	Headers    http.Header
}
type AutoMockList []struct {
	Request  Request
	Response Response
}

type AutoMock struct {
	List      AutoMockList
	listIndex int
	t         *testing.T
}

func NewAutoMock(t *testing.T, list AutoMockList) *AutoMock {
	return &AutoMock{
		List:      list,
		listIndex: 0,
		t:         t,
	}
}

func (m *AutoMock) Do(req *http.Request) (*http.Response, error) {
	assert.Less(m.t, m.listIndex, len(m.List), "AutoMock.Do: list index out of range")
	expected := m.List[m.listIndex]
	m.listIndex++

	assert.Equal(m.t, expected.Request.Method, req.Method)

	// Parse and compare URL
	expectedUrl, err := url.Parse(expected.Request.Url)
	assert.NoError(m.t, err)
	actualUrl, err := url.Parse(req.URL.String())
	assert.NoError(m.t, err)
	assert.Equal(m.t, expectedUrl.String(), actualUrl.String())

	// Compare headers
	for k, v := range expected.Request.Headers {
		assert.Equal(m.t, v, req.Header[k])
	}

	// Compare body
	if expected.Request.Body != nil {
		body, err := ioutil.ReadAll(req.Body)
		assert.NoError(m.t, err)

		// Check content type and compare body data appropriately
		contentType := req.Header.Get("Content-Type")
		if contentType == "application/json" {
			assert.JSONEq(m.t, string(expected.Request.Body), string(body))
		} else if contentType == "application/x-www-form-urlencoded" {
			a, err := url.ParseQuery(string(expected.Request.Body))
			assert.NoError(m.t, err)
			b, err := url.ParseQuery(string(body))
			assert.NoError(m.t, err)
			assert.Equal(m.t, a, b)
		} else {
			assert.Equal(m.t, string(expected.Request.Body), string(body))
		}
	}

	return &http.Response{
		StatusCode: expected.Response.StatusCode,
		Body:       ioutil.NopCloser(bytes.NewReader(expected.Response.Body)),
		Header:     expected.Response.Headers,
	}, nil
}

var (
	// GetDoFunc fetches the mock client's `Do` func
	GetDoFunc func(req *http.Request) (*http.Response, error)
)

func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	return GetDoFunc(req)
}
