TARGETS = clean test
SERVICES = $(wildcard ./services/*)

$(TARGETS): $(SERVICES)
$(SERVICES):
	$(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY: $(TARGETS) $(SERVICES)
